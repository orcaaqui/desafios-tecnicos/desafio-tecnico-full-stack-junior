# Desafio Técnico: full stack júnior

Olá, neste desafio você deve criar uma aplicação em Laravel para facilitar o gerenciamento de horários de uma clínica! A sua aplicação deverá conter:
```
Painel administrativo;
Autenticação de usuários;
Agendamento de consulta;
Cancelamento de consulta;
Listagem de consultas;
Edição de consulta;
Envio de emails.
```
Os dados devem ser salvos em um banco de dados MySQL.
## Funcionalidades:
### Usuários
#### Cadastro
Um usuário poderá se cadastrar como paciente ou médico. 
Pacientes e médicos contém os seguintes dados:
- Nome
- Email
- Data de nascimento

### Autenticação
Pacientes e médicos devem ser autenticados a partir de email e senha, informados no momento do cadastro. 

#### Consulta
### Agendamento
Um paciente pode agendar uma consulta com um médico cadastrado no sistema. Apenas pacientes podem agendar consultas.
Uma consulta contém os seguintes atributos:
- Dia (Ex: 20/12/2020)
- Horário de início (Ex: 12:36)
- Horário de fim (Ex: 13:36)
- Médico

Ao agendar uma consulta, o médico selecionado deve receber um email com os dados da consulta.

### Apagar consulta
Um paciente pode apagar consultas criadas no sistema por si mesmo.
Um médico pode apagar consultas em que foi selecionado.

### Editar consulta
Um paciente pode editar consultas criadas no sistema por si mesmo.

### Listar consultas
Um paciente pode listar todas as consultas criadas por si mesmo.
Um médico pode listar todas as consultas em que foi selecionado.

## Observações
1. Todos os dias às 00:00 todas as consultas que não foram marcadas como "realizada" e passaram do dia marcado devem ser marcadas como "não realizada".

Se você fizer, é um plus:
Evitar conflito de horários nos agendamentos

## Entrega
Você deve enviar um email para contato@orcaaqui.com.br com o repositório do seu projeto. Lembre-se de deixá-lo público.
